#include "LPC11XX.H"
#define KEY1_DOWN() ((LPC_GPIO3->DATA & (1<<0)) != (1<<0))
#define KEY2_DOWN() ((LPC_GPIO3->DATA & (1<<1))!=(1<<1))
unsigned int status;
static volatile unsigned int ticks = 0;
void Delayms(uint16_t ms)
{
	SysTick->CTRL &= ~(1 << 2);
	SysTick->LOAD = SystemCoreClock/2/1000*ms-1;
	SysTick->VAL = 0;
	SysTick->CTRL |= ((1 << 1) | (1 << 0));
	while(! ticks);
	ticks =0 ;
	SysTick->CTRL = 0;
}


void SysTick_Handler(void)
{
	ticks++;
}

void CT32B1_Init(uint32_t interval)
{
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<16);
	LPC_IOCON->R_PIO1_2 &= ~(0X07);
	LPC_IOCON->R_PIO1_2 |= 0X03;
  LPC_SYSCON->SYSAHBCLKCTRL &= ~(1<<16);
  LPC_SYSCON->SYSAHBCLKCTRL |= (1<<10);
	LPC_TMR32B1->TCR = 0X02;
	LPC_TMR32B1->PR = 0;
	LPC_TMR32B1->MCR = 0X02<<9;
	LPC_TMR32B1->PWMC = 0X02;
	LPC_TMR32B1->MR1 = 5*interval/10;
	LPC_TMR32B1->MR3 = interval;
	LPC_TMR32B1->TCR = 0x01;
}

int main(void)
{
	uint16_t i=50;
   CT32B1_Init(SystemCoreClock/1000);
		
	while(1)
	{
		
	
		
			LPC_TMR32B1->MR1 =  (SystemCoreClock/1000/100)*i;
		if(KEY1_DOWN()&&i<90)
    {

       
        i=i+10;
			Delayms(500);
        //LPC_GPIO3->IC = (1<<0);
    }
    if(KEY2_DOWN()&&i>10)
    {

      
        i=i-10;
			Delayms(500);
        //LPC_GPIO3->IC = (1<<1);
    }
	}	
}	